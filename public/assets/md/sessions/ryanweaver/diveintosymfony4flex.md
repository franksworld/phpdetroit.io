Symfony 4 is out! And it's crazy cool. Faster and re-imagined DX with
the all-new Symfony Flex experience.

In this workshop we will:

* **Bootstrap** a Symfony4 app
* Find out what Symfony **Flex** is (the Symfony 4 secret sauce)
* Seamlessly **scale our micro-app into a macro-app** by installing new packages & **recipes**
* Build code super fast with the new **dependency injection** features

... and generally see how much damage we can do (features we can build)
in 4 hours.

Let's rock! 🚀

**Setup Instructions** Please go through the instructions at
[http://bit.ly/symfony4-detroit-workshop](http://bit.ly/symfony4-detroit-workshop) *before*
the workshop so that we can build as much as possible. Message
[Ryan](mailto:hello@knpuniversity.com) if you have any questions!
